library(ggplot2)
library(tibble)
library(corrplot)
library(dplyr)

setwd("C:/Users/naama/Downloads")
hotel.data.raw <- read.csv("hotel_bookings.csv")
df.hotel.prepared<-hotel.data.raw

summary(df.hotel.prepared)
str(df.hotel.prepared)

###############################################################
#             General function coerce_up
###############################################################

coerce_up <- function(x, by){
  if(x<=by) return(x)
  return(by)
}

###############################################################
#                    cleaning data
###############################################################


#correlation - in order to see if some columns can be removed

df.corr <- df.hotel.prepared[,c('lead_time','stays_in_weekend_nights','arrival_date_year','arrival_date_week_number','adr','required_car_parking_spaces',
                             'arrival_date_day_of_month','adults','children','babies','is_repeated_guest','total_of_special_requests',
                             'previous_cancellations','previous_bookings_not_canceled','booking_changes','days_in_waiting_list')]
correlationMatrix<- cor(df.corr, use = "complete.obs")
correlationMatrix
print(corrplot(correlationMatrix, method = "square" , type = "upper",order="original" ,
               tl.cex=0.6, cl.cex =0.6,cl.ratio=0.2 , cl.align.text="c" ,tl.col = "red" , main = "\nCorrelation Plot - without target"))

library(corrplot)
library(caret)

#cutoff 0.85 - there is no features recommended to be removed
highlycorrelated <- findCorrelation(correlationMatrix, cutoff=0.45,names=TRUE,verbose = T)
highlycorrelated 

#is_canceled
str(df.hotel.prepared$is_canceled)
df.hotel.prepared$is_canceled<-factor(df.hotel.prepared$is_canceled, levels = 0:1, labels = c('NO','YES'))
ggplot(df.hotel.prepared, aes(is_canceled))+ geom_bar()+labs(title = "Booking final status",
                                                             x = "is cancelled",
                                                            y = "count")
table(df.hotel.prepared$is_canceled)

#hotel
str(df.hotel.prepared$hotel)
summary(df.hotel.prepared$hotel)
ggplot(df.hotel.prepared, aes(hotel))+ geom_bar() +labs(title = "Hotel Type",
                                                             x = "type",
                                                             y = "count") 


ggplot(df.hotel.prepared, aes(hotel, fill = is_canceled)) + geom_bar(position = 'fill')

##Cancellation Status by Hotel Type##
ggplot(data = df.hotel.prepared,
       aes(
         x = hotel,
         y = prop.table(stat(count)),
         fill = is_canceled,
         label = scales::percent(prop.table(stat(count)))
       )) +
  geom_bar(position = position_dodge()) +
  geom_text(
    stat = "count",
    position = position_dodge(.9),
    vjust = -0.5,
    size = 3
  ) +
  scale_y_continuous(labels = scales::percent) +
  labs(title = "Cancellation Status by Hotel Type",
       x = "Hotel Type",
       y = "Count") +
  theme_classic() 
  
  

#lead_time
str(df.hotel.prepared$lead_time)
summary(df.hotel.prepared$lead_time)
ggplot(df.hotel.prepared,aes(lead_time))+geom_histogram(briwidth=100000)
table(df.hotel.prepared$lead_time)

#binning lead_time
times<-c(0,1,8,31,91,366,Inf)
labels<-c("0","1-7","8-30","31-90","91-365","365+")
bin<-cut(df.hotel.prepared$lead_time,breaks=times,include.lowest=T, right=F,labels=labels)
table(bin)

df.hotel.prepared$lead_time<-bin
ggplot(df.hotel.prepared, aes(lead_time, fill = is_canceled)) + geom_bar(position = 'fill')

#arrival_date_year

ggplot(df.hotel.prepared, aes(arrival_date_year, fill = is_canceled)) + geom_bar(position = 'fill')
df.hotel.prepared$arrival_date_year<-NULL

#arrival_date_month
str(df.hotel.prepared$arrival_date_month)

df.hotel.prepared$arrival_date_month <-
  factor(df.hotel.prepared$arrival_date_month, levels = month.name)

ggplot(df.hotel.prepared, aes(arrival_date_month, fill = is_canceled)) + geom_bar()+  
  geom_text(stat = "count", aes(label = ..count..)) +
  labs(title = "Booking Status by Month",
       x = "Month",
       y = "Count") + theme_bw()


#arrival_date_week_number
ggplot(df.hotel.prepared, aes(arrival_date_week_number, fill = is_canceled)) + geom_bar(position = 'fill')
ggplot(df.hotel.prepared,aes(arrival_date_week_number))+geom_histogram(briwidth=100000)
ggplot(df.hotel.prepared, aes(arrival_date_week_number, colour = is_canceled)) +
  geom_freqpoly(binwidth = 1) + labs(title="Arrival Date Week Number Distribution By is_canceled")

#arrival_date_day_of_month
ggplot(df.hotel.prepared,aes(arrival_date_day_of_month))+geom_histogram(briwidth=100000)
ggplot(df.hotel.prepared, aes(arrival_date_day_of_month, fill = is_canceled)) + geom_bar(position = 'fill')

df.hotel.prepared$arrival_date_day_of_month<-NULL

#stays_in_weekend_nights
table(df.hotel.prepared$stays_in_weekend_nights)
ggplot(df.hotel.prepared, aes(stays_in_weekend_nights)) + geom_bar()
ggplot(df.hotel.prepared, aes(stays_in_weekend_nights, fill = is_canceled)) + geom_bar(position = 'fill')

df.hotel.prepared$stays_in_weekend_nights <- sapply(df.hotel.prepared$stays_in_weekend_nights ,coerce_up,by=5)


#stays_in_week_nights
table(df.hotel.prepared$stays_in_week_nights)
ggplot(df.hotel.prepared, aes(stays_in_week_nights)) + geom_bar()
ggplot(df.hotel.prepared, aes(stays_in_week_nights, fill = is_canceled)) + geom_bar(position = 'fill')

df.hotel.prepared$stays_in_week_nights <- sapply(df.hotel.prepared$stays_in_week_nights ,coerce_up,by=6)

#adults 
table(df.hotel.prepared$adults)
ggplot(df.hotel.prepared, aes(adults, fill = is_canceled)) + geom_bar(position = 'fill')
df.hotel.prepared$adults <- sapply(df.hotel.prepared$adults ,coerce_up,by=4)


#children
str(df.hotel.prepared$children)
table(df.hotel.prepared$children)
summary(df.hotel.prepared$children)

ggplot(df.hotel.prepared, aes(children, fill = is_canceled)) + geom_bar(position = 'fill')


make_0 <- function(x){
  if (is.na(x)) {
    return(0)
  }
  return (x)
}
df.hotel.prepared$children <- sapply(df.hotel.prepared$children ,make_0)

df.hotel.prepared$children <- sapply(df.hotel.prepared$children ,coerce_up,by=2)

#babies 
table(df.hotel.prepared$babies)

make_babies <- function(x){
  if (x>=1) {
    return(as.factor('yes'))
  }
  return (as.factor('no'))
}

df.hotel.prepared$babies <- sapply(df.hotel.prepared$babies ,make_babies)
str(df.hotel.prepared$babies)
ggplot(df.hotel.prepared, aes(babies, fill = is_canceled)) + geom_bar(position = 'fill')

#guests
raw_babies<-hotel.data.raw$babies
df.hotel.prepared$guests<-raw_babies+df.hotel.prepared$children+df.hotel.prepared$adults
df.hotel.prepared$guests <- sapply(df.hotel.prepared$guests ,coerce_up,by=4)
table(df.hotel.prepared$guests)

#droping rows with no guests
df.hotel.prepared <- df.hotel.prepared[!df.hotel.prepared$guests==0,]

ggplot(df.hotel.prepared, aes(guests, fill = is_canceled)) + geom_bar(position = 'fill')

#meal
table(df.hotel.prepared$meal)
ggplot(df.hotel.prepared, aes(meal)) + geom_bar()

make_no_meal <- function(x){
  if (x=='Undefined' || x=='SC') {
    return('No meal')
  }
  return (x)
}
df.hotel.prepared$meal <- as.character(df.hotel.prepared$meal)
str(df.hotel.prepared$meal)
df.hotel.prepared$meal <- sapply(df.hotel.prepared$meal ,make_no_meal)
df.hotel.prepared$meal <- as.factor(df.hotel.prepared$meal)

ggplot(df.hotel.prepared, aes(meal, fill = is_canceled)) + geom_bar(position = 'fill')

#country
table(df.hotel.prepared$country)
ggplot(df.hotel.prepared, aes(country,fill= is_canceled)) +geom_bar(position = 'fill') 
  
filter_country <- df.hotel.prepared %>% 
  group_by(country) %>% 
  filter(n() > 1000)
table(filter_country$country)

table(df.hotel.prepared$country)
ggplot(filter_country, aes(country,fill= is_canceled)) +geom_bar(position = 'fill')

country_vec<- c('AUT','BEL','BRA','CHE','CN','DEU','ESP','FRA','GBR','IRL','ITA','NLD','PRT','SWE','USA')

cut_countries <- function(x, vec){
  
  if(is.element(x, vec)) return(as.character(x))
  return("Other Country")
}
df.hotel.prepared$country<- sapply(df.hotel.prepared$country,cut_countries,country_vec)
ggplot(df.hotel.prepared, aes(country,fill= is_canceled)) +geom_bar(position = 'fill')

country_vec_new<- c(country_vec, 'Other Country')

count_country <- function(x){
  
    return (length(df.hotel.prepared$country[df.hotel.prepared$country==x]))
}


country_length <- sapply(country_vec_new, count_country)

df.hotel.prepared$country<- as.factor(df.hotel.prepared$country)


#pie chart
slices <- country_length
lbls <- country_vec_new
pct <- round((slices/sum(slices))*100)
lbls <- paste(lbls,pct,"%")


pie(slices,labels = lbls, col=rainbow(length(lbls)),
   main="Pie Chart of Countries",radius = 2)

# market_segment
df.hotel.prepared$market_segment

class(df.hotel.prepared$market_segment)

ggplot(df.hotel.prepared, aes(market_segment, fill = is_canceled)) + geom_bar(position = 'fill')

Make.OnlineTA <- function(x){
  if (x=='Undefined') return ('Online TA')
  return (x)
}
df.hotel.prepared$market_segment  <- as.character(df.hotel.prepared$market_segment)
df.hotel.prepared$market_segment <- sapply(df.hotel.prepared$market_segment, Make.OnlineTA)           
df.hotel.prepared$market_segment<-as.factor(df.hotel.prepared$market_segment)          

# distribution_channel- like market_segment
df.hotel.prepared$distribution_channel<- NULL

#previous_cancellations
table(df.hotel.prepared$previous_cancellations)
ggplot(df.hotel.prepared,aes(previous_cancellations))+geom_histogram(briwidth=100000)
ggplot(df.hotel.prepared, aes(previous_cancellations,fill= is_canceled)) +geom_bar(position = 'fill') 

df.hotel.prepared$previous_cancellations <- sapply(df.hotel.prepared$previous_cancellations ,coerce_up,by=1)

#previous_bookings_not_canceled
table(df.hotel.prepared$previous_bookings_not_canceled)
ggplot(df.hotel.prepared,aes(previous_bookings_not_canceled))+geom_histogram(briwidth=100000) 
df.hotel.prepared$previous_bookings_not_canceled <- sapply(df.hotel.prepared$previous_bookings_not_canceled ,coerce_up,by=3)
ggplot(df.hotel.prepared, aes(previous_bookings_not_canceled,fill= is_canceled)) +geom_bar(position = 'fill') 

df.hotel.prepared$previous_bookings_not_canceled<-NULL

#reserved_room_type
str(df.hotel.prepared$reserved_room_type)
table(df.hotel.prepared$reserved_room_type)
ggplot(df.hotel.prepared, aes(reserved_room_type,fill= is_canceled)) +geom_bar(position = 'fill') 

df.hotel.prepared$reserved_room_type<-NULL

#assigned_room_type
table(df.hotel.prepared$assigned_room_type)
df.hotel.prepared$assigned_room_type<-NULL

#booking_changes
table(df.hotel.prepared$booking_changes)
df.hotel.prepared$booking_changes <- sapply(df.hotel.prepared$booking_changes ,coerce_up,by=3)
ggplot(df.hotel.prepared, aes(booking_changes,fill= is_canceled)) +geom_bar(position = 'fill') 


#deposit_type
table(df.hotel.prepared$deposit_type)
ggplot(df.hotel.prepared, aes(deposit_type,fill= is_canceled)) +geom_bar(position = 'fill') 

#agent
table(df.hotel.prepared$agent)
summary(df.hotel.prepared$agent)

Make.agent <- function(x){
  if (x=="NULL") return ('direct')
  return ('by agent')
}

df.hotel.prepared$agent <- as.character(df.hotel.prepared$agent)                  
df.hotel.prepared$agent <- sapply(df.hotel.prepared$agent, Make.agent)       
df.hotel.prepared$agent <- as.factor(df.hotel.prepared$agent)                  

ggplot(df.hotel.prepared, aes(agent,fill= is_canceled)) +geom_bar(position = 'fill') 

#company
table(df.hotel.prepared$company)
summary(df.hotel.prepared$company)
Make.company <- function(x){
  if (x=="NULL") return ('direct')
  return ('by company')
}

df.hotel.prepared$company <- as.character(df.hotel.prepared$company)                  
df.hotel.prepared$company <- sapply(df.hotel.prepared$company, Make.company)       
df.hotel.prepared$company <- as.factor(df.hotel.prepared$company)                  

ggplot(df.hotel.prepared, aes(company,fill= is_canceled)) +geom_bar(position = 'fill') 


#days_in_waiting_list
table(df.hotel.prepared$days_in_waiting_list)
ggplot(df.hotel.prepared,aes(days_in_waiting_list))+geom_histogram(briwidth=1) 

df.hotel.prepared$days_in_waiting_list <- sapply(df.hotel.prepared$days_in_waiting_list ,coerce_up,by=1)
df.hotel.prepared$days_in_waiting_list<-factor(df.hotel.prepared$days_in_waiting_list, levels = 0:1, labels = c('0','1+'))

ggplot(df.hotel.prepared, aes(days_in_waiting_list,fill= is_canceled)) +geom_bar(position = 'fill') 

#customer_type
table(df.hotel.prepared$customer_type)
ggplot(df.hotel.prepared, aes(customer_type,fill= is_canceled)) +geom_bar(position = 'fill') 

#adr - Average Daily Rate
table(df.hotel.prepared$adr)
summary(df.hotel.prepared$adr)
ggplot(df.hotel.prepared,aes(adr))+geom_histogram(briwidth=100)
ggplot(df.hotel.prepared,aes(adr))+geom_histogram(briwidth=100) + xlim(0,500)
ggplot(df.hotel.prepared, aes(as.integer(adr),fill= is_canceled)) +geom_bar(position = 'fill') + xlim(0,400)


#required_car_parking_spaces
table(df.hotel.prepared$required_car_parking_spaces)
str(df.hotel.prepared$required_car_parking_spaces)
df.hotel.prepared$required_car_parking_spaces <- sapply(df.hotel.prepared$required_car_parking_spaces ,coerce_up,by=1)
ggplot(df.hotel.prepared, aes(required_car_parking_spaces,fill= is_canceled)) +geom_bar(position = 'fill') 

df.hotel.prepared$required_car_parking_spaces<-factor(df.hotel.prepared$required_car_parking_spaces, levels = 0:1, labels = c('0','1+'))

#total_of_special_requests
table(df.hotel.prepared$total_of_special_requests)
df.hotel.prepared$total_of_special_requests <- sapply(df.hotel.prepared$total_of_special_requests ,coerce_up,by=3)
ggplot(df.hotel.prepared, aes(total_of_special_requests,fill= is_canceled)) +geom_bar(position = 'fill') 

# reservation_status
table(df.hotel.prepared$reservation_status,df.hotel.prepared$is_canceled)

ggplot(df.hotel.prepared, aes(reservation_status, fill = is_canceled)) + geom_bar()

df.hotel.prepared$reservation_status <- NULL

#  reservation_status_date
df.hotel.prepared$reservation_status_date <- NULL

###############################################################
#              Train Test split
###############################################################
library(caTools)

filter <- sample.split(df.hotel.prepared$is_canceled, SplitRatio = 0.7)
hotel.train <- subset(df.hotel.prepared, filter == T)
hotel.test <- subset(df.hotel.prepared, filter == F)

###############################################################
#              Decision tree
###############################################################

library(rpart)
library(rpart.plot)


model.dt <- rpart(is_canceled ~ ., hotel.train)
rpart.plot(model.dt, box.palette = "RdBu", shadow.col = "gray", nn = TRUE)

prediction.dt <- predict(model.dt,hotel.test)
actual.dt <- hotel.test$is_canceled
prediction.dt <- prediction.dt[,'YES']

hist(prediction.dt)

cf_dt <- table(actual.dt,prediction.dt>0.5)

precision.dt <- cf_dt[2,2]/(cf_dt[2,2] + cf_dt[1,2])
recall.dt <- cf_dt[2,2]/(cf_dt[2,2] + cf_dt[2,1])
accuricy.dt<- (cf_dt[2,2]+cf_dt[1,1])/length(hotel.test$is_canceled)

###############################################################
#              Decision tree whithout deposit_type
###############################################################

hotel.data<-df.hotel.prepared
str(hotel.data)
hotel.data$deposit_type<-NULL

filter <- sample.split(hotel.data$is_canceled, SplitRatio = 0.7)
hotel.data.train <- subset(hotel.data, filter == T)
hotel.data.test <- subset(hotel.data, filter == F)

model.dt.without <- rpart(is_canceled ~ ., hotel.data.train)
rpart.plot(model.dt.without, box.palette = "RdBu", shadow.col = "gray", nn = TRUE)

prediction.without <- predict(model.dt.without,hotel.data.test)
actual.without <- hotel.data.test$is_canceled
prediction.without <- prediction.without[,'YES']

hist(prediction.without)

cf_dt_w <- table(actual.without,prediction.without>0.5)

precision.dt.without <- cf_dt_w[2,2]/(cf_dt_w[2,2] + cf_dt_w[1,2])
recall.dt.without <- cf_dt_w[2,2]/(cf_dt_w[2,2] + cf_dt_w[2,1])
accuricy.dt.without<- (cf_dt_w[2,2]+cf_dt_w[1,1])/length(hotel.data.test$is_canceled)

###############################################################
#              random forest
###############################################################

library(randomForest)

summary(df.hotel.prepared)
model.rf <- randomForest(is_canceled ~ ., data = hotel.train, importance = TRUE)

#Examine the importance of the features
importance(model.rf)

# make dataframe from importance() output
feat_imp_df <- importance(model.rf) %>% 
  data.frame() %>% 
  mutate(feature = row.names(.)) 

# plot dataframe
ggplot(feat_imp_df, aes(x = reorder(feature, MeanDecreaseGini), 
                        y = MeanDecreaseGini)) +
  geom_bar(stat='identity') +
  coord_flip() +
  theme_classic() +
  labs(
    x     = "Feature",
    y     = "Importance",
    title = "Feature Importance For RF model"
  )


prediction.rf <- predict(model.rf,hotel.test,type="prob")
prediction.rf <- prediction.rf[,'YES']

hist(prediction.rf)

actual.rf <- hotel.test$is_canceled
cf.rf <- table(actual.rf,prediction.rf > 0.5)

precision.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[1,2])
recall.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[2,1])
accuricy.rf<- (cf.rf[2,2]+cf.rf[1,1])/length(hotel.test$is_canceled)

###############################################################
#             Logistic regresion
###############################################################

str(hotel.train)
hotel.model.glm <- glm(is_canceled ~ ., family = binomial(link = "logit"), data = hotel.train)

prediction.glm <- predict(hotel.model.glm, newdata = hotel.test, type = 'response')

hist(prediction.glm)

actual.glm <- hotel.test$is_canceled
cf_glm <- table(actual.glm,prediction.glm>0.5)

precision.glm <- cf_glm[2,2] /(cf_glm[2,2] + cf_glm[1,2])
recall.glm <- cf_glm[2,2] /(cf_glm[2,2] + cf_glm[2,1])
accuracy.glm <- (cf_glm[2,2] + cf_glm[1,1])/length(hotel.test$is_canceled)


###############################################################
#              KNN preparation
###############################################################
df.hotel<-df.hotel.prepared
str(df.hotel)

#adding back and convert valuable features
df.hotel$lead_time <- as.numeric(df.hotel$lead_time)
df.hotel$arrival_date_month <- as.numeric(df.hotel$arrival_date_month)
df.hotel$adr <- as.numeric(df.hotel$adr)
df.hotel$country <- as.numeric(df.hotel$country)
df.hotel$market_segment <- as.numeric(df.hotel$market_segment)
df.hotel$deposit_type <- as.numeric(df.hotel$deposit_type)
df.hotel$required_car_parking_spaces <- as.numeric(df.hotel$required_car_parking_spaces)

#filtered down the predictor variables
hotel.subset<-df.hotel[c(-1,-10,-11,-18,-19,-20,-21)]
head(hotel.subset)
str(hotel.subset)

#Data Normalization
normalize <- function(x) {
  return ((x - min(x)) / (max(x) - min(x))) }

hotel.norm <- as.data.frame(lapply(hotel.subset[,2:10], normalize))
head(hotel.norm)
summary(hotel.norm)

# split to train test
split<-sample.split(hotel.norm$lead_time,SplitRatio = 0.7)
hotel.train.knn<-subset(hotel.norm,split==T)
hotel.test.knn<-subset(hotel.norm,split==F)

dim(hotel.train.knn)
dim(hotel.test.knn)

#Creating seperate vectors for 'is_canceled' feature which is our target for the train&test set.
is_canceled_train <- subset(hotel.subset$is_canceled,split==T)
length(is_canceled_train)
is_canceled_test <- subset(hotel.subset$is_canceled,split==F)
length(is_canceled_test)


###############################################################
#              KNN model
###############################################################

library(class)
library(e1071)

knn.train.pred <- knn(train=hotel.train.knn, test=hotel.test.knn, is_canceled_train, k=289)
length(knn.train.pred)

cf_knn <- table(is_canceled_test,knn.train.pred)

TP <- cf_knn[2,2]
FP <- cf_knn[1,2]
TN <- cf_knn[1,1]
FN <- cf_knn[2,1]

precision.knn <- TP/(TP +FP)
recall.knn <- TP/(TP + FN)
accuracy.knn <- (TP + TN) / length(knn.train.pred)


# find the best k (the best ouput was 289)
#LONG RUN - therefor it is mark with "hash marks"
#i=1                         
#k.optm=1 
#range <- (280:300)
#for (i in range){ 
#  knn.mod <-  knn(train=hotel.train.knn, test=hotel.test.knn, cl=is_canceled_train, k=i)
#  k.optm[i] <- 100 * sum(is_canceled_test == knn.mod)/NROW(is_canceled_test)
#  k=i  
#  cat(k,'=',k.optm[i],'\n')       # to print % accuracy 
#}

###############################################################
#              Naive bayes
###############################################################

library(caret)

model.nb<-naiveBayes(is_canceled ~ .,data = hotel.train)
model.nb # we can check her all the conditional probabilities

prediction.nb <- predict(model.nb,hotel.test, type = "raw")
prediction.nb <- prediction.nb[,'YES']
actual.nb <- hotel.test$is_canceled

hist(prediction.nb)

confusion_matrix_NB <- table(actual.nb,prediction.nb > 0.5)

TP <- confusion_matrix_NB[2,2]
FP <- confusion_matrix_NB[1,2]
TN <- confusion_matrix_NB[1,1]
FN <- confusion_matrix_NB[2,1]

precision.nb <- TP/(TP +FP)
recall.nb <- TP/(TP + FN)
accuracy.nb <- (TP + TN) / length(hotel.test$is_canceled)

summary(prediction.nb)

###############################################################
#              XgBoost
###############################################################
library(magrittr)
library(Matrix)
library(xgboost)

train <- hotel.train
test <- hotel.test

make_int <- function(x){
  if (x=="YES") return (1)
  return (0)
}
train$is_canceled <- sapply(train$is_canceled,make_int)
test$is_canceled <- sapply(test$is_canceled,make_int)
str(train$is_canceled)

trainm <- sparse.model.matrix(is_canceled ~.-1,data = train)
head(trainm)
train_label <- train[,'is_canceled']
train_matrix <- xgb.DMatrix(data = as.matrix(trainm), label = train_label)

testm <- sparse.model.matrix(is_canceled ~.-1,data = test)
head(testm)
test_label <- test[,'is_canceled']
test_matrix <- xgb.DMatrix(data = as.matrix(testm), label = test_label)

nc <- length(unique(train_label))
xgb_params <- list("objective" = "multi:softprob",
                   "eval_metric" = "mlogloss",
                   "num_class" = nc) 
watchlist <- list(train = train_matrix, test = test_matrix)

#eXtreme Gradient Boosting Model
bst_model <- xgb.train(params = xgb_params,
                       data = train_matrix,
                       nrounds = 474,
                       watchlist = watchlist ) #out of 500 nrounds

#Training & Test error plot
e <- data.frame(bst_model$evaluation_log)
plot(e$iter, e$train_mlogloss, col= 'blue')
lines(e$iter, e$test_mlogloss, col = 'red')

min(e$test_mlogloss)
e[e$test_mlogloss == 0.286273,] #474

#Feature Importance
imp <- xgb.importance(colnames(train_matrix), model = bst_model)
print(imp)
xgb.plot.importance(imp)

#Prediction $ confusion matrix
prediction.xgboost <- predict(bst_model, newdata = test_matrix)
str(prediction.xgboost)

pred <- matrix(prediction.xgboost, nrow = nc, ncol = length(prediction.xgboost)/nc) %>%
               t() %>%
               data.frame() %>%
               mutate(label = test_label, max_prob = max.col(.,"last")-1)

seq_vec <- seq(2,71526,by=2)
prediction.xgb <- prediction.xgboost[seq_vec]
str(prediction.xgb)

hist(prediction.xgb)

actual.xgb <- test$is_canceled
length(actual.xgb)
length(prediction.xgb)
cf_xgb <- table(actual.xgb,prediction.xgb>0.5)

TP <- cf_xgb[2,2]
FP <- cf_xgb[1,2]
TN <- cf_xgb[1,1]
FN <- cf_xgb[2,1]

precision.xgb <- TP/(TP +FP)
recall.xgb <- TP/(TP + FN)
accuracy.xgb <- (TP + TN) / length(prediction.xgb)


###############################################################
#              ROC chart
###############################################################
library(pROC)

rocCurveDT <- roc(actual.dt,prediction.dt, direction = ">", levels = c("YES", "NO"))
rocCurveDT_without <- roc(actual.without,prediction.without, direction = ">", levels = c("YES", "NO"))
rocCurveRF <- roc(actual.rf,prediction.rf, direction = ">", levels = c("YES", "NO"))
rocCurveGLM <- roc(actual.glm,prediction.glm, direction = ">", levels = c("YES", "NO"))
rocCurveknn <- roc(is_canceled_test,as.vector(knn.train.pred, mode = "numeric"), direction = ">", levels = c("YES", "NO"))
rocCurveNB <- roc(actual.nb,prediction.nb, direction = ">", levels = c("YES", "NO"))
rocCurveXGB <- roc(actual.xgb,prediction.xgb, direction = ">", levels = c(1,0))

plot(rocCurveDT, col = 'red',main = "ROC Chart")
par(new = TRUE)
plot(rocCurveDT_without, col = 'purple',main = "ROC Chart")
par(new = TRUE)
plot(rocCurveRF, col = 'blue',main = "ROC Chart")
par(new = TRUE)
plot(rocCurveGLM, col = 'green',main = "ROC Chart")
par(new = TRUE)
plot(rocCurveknn, col = 'yellow',main = "ROC Chart")
par(new = TRUE)
plot(rocCurveNB, col = 'turquoise',main = "ROC Chart")
par(new = TRUE)
plot(rocCurveXGB, col = 'brown',main = "ROC Chart")

auc(rocCurveDT)
auc(rocCurveDT_without)
auc(rocCurveRF)
auc(rocCurveGLM)
auc(rocCurveknn)
auc(rocCurveNB)
auc(rocCurveXGB)

###############################################################
#           Calculating indices & conclusions
###############################################################

eco.hotel<-hotel.data.raw

#adr - Average Daily Rate
#assuming that only adults & children are paying guests.
adr_per_guest <- mean(df.hotel.prepared$adr/(df.hotel.prepared$adults+df.hotel.prepared$children))
adr_per_reservation <- mean(df.hotel.prepared$adr)

#we want to find out the average days of reservation
eco.hotel$stay_nights <- eco.hotel$stays_in_weekend_nights + eco.hotel$stays_in_week_nights
avg.stay.nights <- mean(eco.hotel$stay_nights)

#we want to find out the average number of guests
avg.guests <- mean(df.hotel.prepared$guests)


# Average daily rate by Hotel Type
ggplot(df.hotel.prepared, aes(x = adr, fill = hotel, color = hotel)) + 
  geom_histogram(aes(y = ..density..), position = position_dodge(), binwidth = 20 ) +
  geom_density(alpha = 0.2) + 
  labs(title = "Average Daily rate by Hotel",
       x = "Hotel Price (Euro)",
       y = "Count") + scale_color_brewer(palette = "Paired") + 
  theme_classic() + theme(legend.position = "top") + xlim(0,600)

# we want to find out the monthly booking statistics.
eco.hotel$arrival_date_month <-  factor(eco.hotel$arrival_date_month, levels = month.name)

ggplot(eco.hotel, aes(arrival_date_month)) + geom_bar()+  
  geom_text(stat = "count", aes(label = ..count..)) +
  labs(title = "Booking Count by Month",
       x = "Month",
       y = "Count") + theme_bw()
#Monthly Income 
df_actual_arrived <-  df.hotel.prepared
df_actual_arrived <- df_actual_arrived[df_actual_arrived$is_canceled=='NO',]

monthly_income <- data.frame(df_actual_arrived  %>% 
                               group_by(arrival_date_month) %>%
                               summarise(sum(adr)))

ggplot(monthly_income, aes(x=arrival_date_month,y=sum.adr.,group=1)) + geom_point(shape=21, color="black", fill="#69b3a2", size=6)+
  geom_path()+
  labs(title = "Monthly Income",
       x = "Month",
       y = "Income") + theme_bw()

# 
library(anytime)
eco.hotel$reservation_status_date<-as.Date(eco.hotel$reservation_status_date)

#create a new column for arrivel date
eco.hotel$arrivel_date<-paste(eco.hotel$arrival_date_day_of_month,eco.hotel$arrival_date_month,eco.hotel$arrival_date_year)
eco.hotel$arrivel_date<-anydate(eco.hotel$arrivel_date)
eco.hotel$arrivel_date<-as.Date(eco.hotel$arrivel_date)
table(eco.hotel$arrivel_date)

#create a new dataFrame and new column range
cancelled.date <- eco.hotel %>% filter(reservation_status== "Canceled")
cancelled.date$range<-cancelled.date$arrivel_date-cancelled.date$reservation_status_date

ggplot(cancelled.date,aes(range))+geom_histogram(briwidth=1000)
str(cancelled.date)
table(cancelled.date$range)
summary(cancelled.date$range)

#plot cancellation range feature
ggplot(cancelled.date, aes(x = range)) + 
  geom_histogram(aes(y = ..density..), binwidth = 5 ) +
  geom_density(alpha = 0.2) + 
  labs(title = "Distribution of days from Cancellation to arrive dates (Range)",
       x = "range days",
       y = "%") + scale_color_brewer(palette = "Paired") + 
  theme_classic() + theme(legend.position = "top") + xlim(-20,550)

# we want to find out the rate of cancelled reservation in a week from all the range cancellations : almost 12%
hotel.cancellation.7 <- cancelled.date %>% filter(range<=7)
rate.less.7 <- length(hotel.cancellation.7$range) / length(cancelled.date$range)

# which market segment type cancelled less than 7 days before arrive
library(scales)

df.ms <- data.frame(hotel.cancellation.7 %>%
                   group_by(market_segment) %>%
                   summarise(count = n()) )


ggplot(df.ms, aes(x="", y=count, fill=market_segment))+
geom_bar(width = 1, stat = "identity") + coord_polar("y", start=0) 


# which customer type cancelled less than 7 days before arrive
df.customer_type <- data.frame(hotel.cancellation.7 %>%
                   group_by(customer_type,market_segment,adr) %>%
                   summarise(count = n()) )

ggplot(df.customer_type, aes(x="", y=count, fill=customer_type))+
  geom_bar(width = 1, stat = "identity") + coord_polar("y", start=0)


###############################################################
#             Econimic Analize & model
###############################################################

#find the optimum boundry level (bl) for our models - according to The True Costs we have calculated.

avg.res.price <- adr_per_reservation * avg.stay.nights #349 Euro

#FP_cost = the average cost of cancelled reservation 
FP_cost <- avg.res.price
cost_more_than_7 <- 0.5*FP_cost
#FN_cost = the average cost to confirm reservation that will be cancelled
FN_cost <- (avg.res.price * rate.less.7) + (1-rate.less.7) * cost_more_than_7 #195 Euro

#A genral function to compute revenue 
compute.revenue <- function(bl,cost1,cost2,price,actual,prediction){
  confusion_matrix <- table(actual, prediction >bl)
  cost <- confusion_matrix[1,2]*cost1 + confusion_matrix[2,1]*cost2
  income <- confusion_matrix[1,1]*price
  revenue <- income-cost
  return (revenue)
}

compute.revenue(0.5,FP_cost,FN_cost,avg.res.price,actual.rf,prediction.rf)

#Compute optimum boundary layer as a fucntion of cost 

#vector for computing the optimum 
bl.vec <- seq(0,1,length.out = 51)
bl.vec <- bl.vec[2:50]

#Comnpute optimum 
results <- sapply(bl.vec,compute.revenue, cost1=FP_cost, cost2=FN_cost, price=avg.res.price, actual=actual.rf, prediction=prediction.rf)
plot(bl.vec, results)

#A general function to compute optimum 
findmax <- function(FUN, vec, cost1, cost2, price, actual, prediction){
  max = -10000000
  for (item in vec){
    if (FUN(item, cost1, cost2,price, actual, prediction) > max) {
      max <- FUN(item, cost1, cost2,price, actual, prediction)
      bestitem <- item
    }  
  }
  return (c(bestitem,max))
}

#calculate Max revenue & BL - for our top 3 models:

#XgBoost 
findmax(compute.revenue,bl.vec,FP_cost,FN_cost,avg.res.price,actual.xgb,prediction.xgb)

#Random Forest 
findmax(compute.revenue,bl.vec,FP_cost,FN_cost,avg.res.price,actual.rf,prediction.rf) 

#Logistic Regression
findmax(compute.revenue,bl.vec,FP_cost,FN_cost,avg.res.price,actual.glm,prediction.glm) 


#total error vs. base level 
#checking if our model improve the rate of errors.
#our most profitable model is the XgBoost

cf_xgb <- table(actual.xgb,prediction.xgb>0.8) # XgBoost confution martrix
#total errors
total_errors_xgb <- (cf_xgb[2,1]+cf_xgb[1,2])/length(hotel.test$is_canceled)
#base level
num_Cancelled <- dim(df.hotel.prepared[df.hotel.prepared$is_canceled=='YES',])[1]
base.level <- num_Cancelled / dim(df.hotel.prepared)[1]
#Model improved errors from 37% in base level to 16% with model (total error)


#ROI

calculateROI <- function(invest,expense,income,rate){
  income <- income+income/(1+rate)+income/(1+rate)^2
  expense <- invest+expense+expense/(1+rate)+expense/(1+rate)^2
  profit <- income-expense
  ROI <- (profit/expense)
  return (c(profit,ROI))
}

## if ROI > rate we will invest in the model

# example using the function
calculateROI(400000,15000,269250,0.05)

